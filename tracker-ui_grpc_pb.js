// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var tracker$ui_pb = require('./tracker-ui_pb.js');
var google_protobuf_timestamp_pb = require('google-protobuf/google/protobuf/timestamp_pb.js');

function serialize_ui_GetConfigReq(arg) {
  if (!(arg instanceof tracker$ui_pb.GetConfigReq)) {
    throw new Error('Expected argument of type ui.GetConfigReq');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_ui_GetConfigReq(buffer_arg) {
  return tracker$ui_pb.GetConfigReq.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ui_GetConfigResp(arg) {
  if (!(arg instanceof tracker$ui_pb.GetConfigResp)) {
    throw new Error('Expected argument of type ui.GetConfigResp');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_ui_GetConfigResp(buffer_arg) {
  return tracker$ui_pb.GetConfigResp.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ui_GetEventsReq(arg) {
  if (!(arg instanceof tracker$ui_pb.GetEventsReq)) {
    throw new Error('Expected argument of type ui.GetEventsReq');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_ui_GetEventsReq(buffer_arg) {
  return tracker$ui_pb.GetEventsReq.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ui_GetEventsResp(arg) {
  if (!(arg instanceof tracker$ui_pb.GetEventsResp)) {
    throw new Error('Expected argument of type ui.GetEventsResp');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_ui_GetEventsResp(buffer_arg) {
  return tracker$ui_pb.GetEventsResp.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ui_GetIsConfiguredReq(arg) {
  if (!(arg instanceof tracker$ui_pb.GetIsConfiguredReq)) {
    throw new Error('Expected argument of type ui.GetIsConfiguredReq');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_ui_GetIsConfiguredReq(buffer_arg) {
  return tracker$ui_pb.GetIsConfiguredReq.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ui_GetIsConfiguredResp(arg) {
  if (!(arg instanceof tracker$ui_pb.GetIsConfiguredResp)) {
    throw new Error('Expected argument of type ui.GetIsConfiguredResp');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_ui_GetIsConfiguredResp(buffer_arg) {
  return tracker$ui_pb.GetIsConfiguredResp.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ui_GetVideoEventsReq(arg) {
  if (!(arg instanceof tracker$ui_pb.GetVideoEventsReq)) {
    throw new Error('Expected argument of type ui.GetVideoEventsReq');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_ui_GetVideoEventsReq(buffer_arg) {
  return tracker$ui_pb.GetVideoEventsReq.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ui_GetVideoEventsResp(arg) {
  if (!(arg instanceof tracker$ui_pb.GetVideoEventsResp)) {
    throw new Error('Expected argument of type ui.GetVideoEventsResp');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_ui_GetVideoEventsResp(buffer_arg) {
  return tracker$ui_pb.GetVideoEventsResp.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ui_SetConfigReq(arg) {
  if (!(arg instanceof tracker$ui_pb.SetConfigReq)) {
    throw new Error('Expected argument of type ui.SetConfigReq');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_ui_SetConfigReq(buffer_arg) {
  return tracker$ui_pb.SetConfigReq.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ui_SetConfigResp(arg) {
  if (!(arg instanceof tracker$ui_pb.SetConfigResp)) {
    throw new Error('Expected argument of type ui.SetConfigResp');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_ui_SetConfigResp(buffer_arg) {
  return tracker$ui_pb.SetConfigResp.deserializeBinary(new Uint8Array(buffer_arg));
}


var UiService = exports.UiService = {
  setConfig: {
    path: '/ui.Ui/SetConfig',
    requestStream: false,
    responseStream: false,
    requestType: tracker$ui_pb.SetConfigReq,
    responseType: tracker$ui_pb.SetConfigResp,
    requestSerialize: serialize_ui_SetConfigReq,
    requestDeserialize: deserialize_ui_SetConfigReq,
    responseSerialize: serialize_ui_SetConfigResp,
    responseDeserialize: deserialize_ui_SetConfigResp,
  },
  getConfig: {
    path: '/ui.Ui/GetConfig',
    requestStream: false,
    responseStream: false,
    requestType: tracker$ui_pb.GetConfigReq,
    responseType: tracker$ui_pb.GetConfigResp,
    requestSerialize: serialize_ui_GetConfigReq,
    requestDeserialize: deserialize_ui_GetConfigReq,
    responseSerialize: serialize_ui_GetConfigResp,
    responseDeserialize: deserialize_ui_GetConfigResp,
  },
  getIsConfigured: {
    path: '/ui.Ui/GetIsConfigured',
    requestStream: false,
    responseStream: false,
    requestType: tracker$ui_pb.GetIsConfiguredReq,
    responseType: tracker$ui_pb.GetIsConfiguredResp,
    requestSerialize: serialize_ui_GetIsConfiguredReq,
    requestDeserialize: deserialize_ui_GetIsConfiguredReq,
    responseSerialize: serialize_ui_GetIsConfiguredResp,
    responseDeserialize: deserialize_ui_GetIsConfiguredResp,
  },
  getEvents: {
    path: '/ui.Ui/GetEvents',
    requestStream: false,
    responseStream: false,
    requestType: tracker$ui_pb.GetEventsReq,
    responseType: tracker$ui_pb.GetEventsResp,
    requestSerialize: serialize_ui_GetEventsReq,
    requestDeserialize: deserialize_ui_GetEventsReq,
    responseSerialize: serialize_ui_GetEventsResp,
    responseDeserialize: deserialize_ui_GetEventsResp,
  },
  getVideoEvents: {
    path: '/ui.Ui/GetVideoEvents',
    requestStream: false,
    responseStream: false,
    requestType: tracker$ui_pb.GetVideoEventsReq,
    responseType: tracker$ui_pb.GetVideoEventsResp,
    requestSerialize: serialize_ui_GetVideoEventsReq,
    requestDeserialize: deserialize_ui_GetVideoEventsReq,
    responseSerialize: serialize_ui_GetVideoEventsResp,
    responseDeserialize: deserialize_ui_GetVideoEventsResp,
  },
};

exports.UiClient = grpc.makeGenericClientConstructor(UiService);
